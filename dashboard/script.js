const holes = document.querySelectorAll('.hole');
const scoreBoard = document.querySelector('.score');
const moles = document.querySelectorAll('.mole');
const mallet = document.querySelectorAll('.mallet');
let scoreOne = document.querySelectorAll('.highOne');
let lastHole; 
let timeUp = false; 


//test localstorage here 
const form = document.querySelector('form');
const ul = document.querySelector('ul');
const button = document.querySelector('button');
const input = document.getElementById('item');
let nameList = localStorage.getItem('items') ? JSON.parse(localStorage.getItem('items')) : [];

localStorage.setItem('items', JSON.stringify(nameList)); 
const data = JSON.parse(localStorage.getItem('items'));

function randomTime(min, max) {
    return Math.round(Math.random() * (max - min) + min); // get a random time for the spawn 
}

function randomHole(holes) {
    const idx = Math.floor(Math.random() * holes.length); // get random number for each hole /6
    const hole = holes[idx];
    if (hole === lastHole) { // dont spawn the mole in the same hole twice 
        //console.log('same hole you idiot');
        return randomHole(holes);
    }
    lastHole = hole; 
    return hole;  
}

function peep() {
    const time = randomTime(200, 1000); // this is the time beteen mole spawns, can change to higher for easier game etc
    const hole = randomHole(holes); // call the math function that determines the random hole 
    hole.classList.add('up'); // spawn the mole
    setTimeout(() => {
        hole.classList.remove('up'); 
       if(!timeUp) 
       peep();
    }, time);
}

function startGame() { 
    scoreBoard.textContent = 0; // set score to 0 
    timeUp = false; // reset time incase it is whack 
    score = 0; // reset score 
    peep(); // run the game 
    setTimeout(() => timeUp = true, 10000) // set game timer to 10 seconds 
    //updateScore(); // comment this out while I try to get leaderboard working     
}

function hit(event) {
    if(!event.isTrusted) return; //catch fake mouse clicks 
    score++; // increment score 
    this.classList.remove('up'); // remove the mole that was hit 
    scoreBoard.textContent = score; //add the value to the scoreBoard, from .score in css 
    //console.log(name, score); 
    //mallet.style.transform = 'rotate(90deg)';   
}

const liMaker = (text) => {
    const li = document.createElement('li');
    li.textContent = text;
    ul.appendChild(li);
  }
  
function saveScore() {
    nameList.push(input.value + ' ' + score);
    localStorage.setItem('items', JSON.stringify(nameList));
    liMaker(input.value);
    input.value = "";
    nameList.sort(); //not working 
    nameList.slice(0, 10); //not working 
    location.reload(); 
  };
  
  data.forEach(item => {
    liMaker(item);
  });
  
//   button.addEventListener('click', function () {
//     localStorage.clear();
//     while (ul.firstChild) {
//       ul.removeChild(ul.firstChild);
//     }
//     nameList = [];
//   });

function highScores() { // in progress 
    if(typeof(Storage)!=="undefined") {
        var score = false; 
        if(localStorage["highOne"]) {
            scoreOne.style.display = "block";
            scoreOne.innerHTML = '';
        }
    } else {
        scoreOne.style.display = "none"; 
    }
}

function updateScore() { // in progress 
    document.getElementById("highOne").innerHTML = ("1. " + name + " " + score); 
    document.getElementById("high2").innerHTML = ("2. " + name + " " + score); 
    document.getElementById("high3").innerHTML = ("3. " + name + " " + score); 
    highScores();
}

moles.forEach(mole => mole.addEventListener('click', hit)); //add event listener to each mole, from css class 